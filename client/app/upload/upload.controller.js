(function () {
    angular
        .module("UploadApp")
        .controller("UploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ["UploadService"];

    function UploadCtrl(UploadService) {
        var vm = this;
		vm.imgFile = null;
		vm.comment = "";
		vm.status = {
			message: "",
			code: 0
		};
		vm.content = [];

        vm.upload = function(){

            UploadService
                .uploadToAlbum({
					data: {
						"img-file": vm.imgFile,
						"comment": vm.comment
					}
				}).then(function (resp){
					vm.imgFile = null;
					vm.comment = "";
					vm.status.message = "The album is saved successfull";
					vm.status.code = 202;
				}).catch(function (err){
					vm.status.message = "Fail to save the album.";
					vm.status.code = 400;
				});
		};

		vm.refreshAlbum = function(){
			UploadService
				.refreshAlbum()
				.then(function(albums){
					vm.albums = albums;
				})
		};



        vm.deleteFromAlbum = function(){
            UploadService
                .deleteFromAlbum()
				.then(function() {
					vm.albums = [];
					vm.status.message = "Your album is empty now."
					vm.status.code = 202;
				})
				.catch(function (){
					vm.status.message = "Fail to delete your album."
					vm.status.code = 400;
				})
        };
    }
})();
