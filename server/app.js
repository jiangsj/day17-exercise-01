//Load express
var express = require("express");

//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var session = require("express-session");

app.use(session({
    secret: "1111",
    resave: false,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    //TODO : initialize your session here with empty Array
    if(!req.session.album){
        req.session.album = [];
    }
    next();
});


/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */
app.use(express.static(__dirname + "/../client"));
app.use(express.static(__dirname + "/../client/app/images"));

require("./route")(app);

//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});