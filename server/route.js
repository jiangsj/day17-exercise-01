module.exports = function (app) {

    var fs = require("fs");
    var path = require("path");

    var multer = require("multer");

    var storage = multer.diskStorage({
        destination: __dirname + "/../client/images",
        filename: function (req, file, cb) {
            cb(null, file.originalname)
        }
    })

    var multipart = multer({storage: storage});

    app.post("/api/album", multipart.single("img-file"), function (req, res) {
        //TODO : insert into your session with Album Data Here
		console.log('req.body', req.body.comment);
		console.log('req.file', req.file.originalname);
        req.session.album.push({
			filePath: req.file.originalname,
			conmment: req.body.comment
		});
        fs.readFile(req.file.path, function(err, data){
            res.send(200, req.file.size);
        });
    });
    //TODO : write here the other routing to ADD and DELETE from Album
	app.get("/api/album", function(req, res){
		console.log("get", req.session);
		res.status(200).json(req.session.album);
	});



	app.delete("/api/album", function(req, res){
		console.log("delete", req.session);
		req.session.destroy();
		res.status(200).end();
	});

}
